import { useState } from "react";
import { withRouter } from "react-router-dom";

const ContactDetail = (props) => {
  const [contactName, setCntactName] = useState(props.match.params.chatName);

  const redirectToChat = () => {
    props.history.push(`/${props.match.params.chatName}`);
  };

  return (
    <div className="contactDetail">
      <h1 className="contactDetail__title">{contactName}</h1>
      <button className="contactDetail__btn" onClick={redirectToChat}>
        Message
      </button>
    </div>
  );
};

export default withRouter(ContactDetail);
