import React, { useState } from "react";
import Header from "../../components/Header";
import ChatList from "../../components/ChatList";
import MessagesList from "../../components/MessagesList";
import ChatContext from "../../context/ChatContext";
import ContactsModal from "../../components/Modal";

const Home = () => {
  const [openContacts, setOpenContacts] = useState(false);
  const [openChats, setOpenChats] = useState(false);
  const [chats, setChats] = useState([
    { name: "yasaman", message: "" },
    { name: "ali", message: "" },
    { name: "narin", message: "" },
    { name: "rahmat", message: "" },
  ]);

  const changeMessage = (chatName) => {
    let chatHistory = JSON.parse(localStorage.getItem(chatName));
    if (chatHistory) {
      let newChats = [...chats];
      if (chatHistory[chatHistory.length - 1]) {
        newChats.find((c) => c.name === chatName).message =
          chatHistory[chatHistory.length - 1].content;
        setChats(newChats);
      }
    }
  };

  const handleOpenContacts = () => {
    setOpenContacts(!openContacts);
  };

  const handleOpenChats = () => {
    setOpenChats(!openChats);
  };

  return (
    <ChatContext.Provider value={{ chats, changeMessage }}>
      <Header openContacts={handleOpenContacts} openChat={handleOpenChats} />
      {openContacts && <ContactsModal toggleModal={handleOpenContacts} />}
      {openChats && (
        <ContactsModal toggleModal={handleOpenChats} isChat={true} />
      )}
      <div className="container">
        <div className="hide-in-touch">
          <ChatList isChat={true} />
        </div>
        <MessagesList />
      </div>
    </ChatContext.Provider>
  );
};

export default Home;
