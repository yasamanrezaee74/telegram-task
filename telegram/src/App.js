import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";
import "./assets/sass/index.scss";

function App() {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
}

export default App;
