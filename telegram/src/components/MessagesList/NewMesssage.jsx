import { useState } from "react";

const NewMesssage = ({ message, onChange, onSubmit }) => {
  return (
    <form className="newMess__form" onSubmit={onSubmit}>
      <input
        autoFocus
        autoComplete="off"
        className="newMess__input"
        value={message}
        onChange={onChange}
        name="message"
      />
      <button className="newMess__btn" type="submit">
        send
      </button>
    </form>
  );
};

export default NewMesssage;
