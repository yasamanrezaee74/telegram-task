import React, { useState, useEffect, useContext } from "react";
import { withRouter } from "react-router-dom";
import NewMesssage from "./NewMesssage";
import ChatContext from "../../context/ChatContext";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const MessagesList = (props) => {
  const messagesEndRef = React.createRef();
  const { changeMessage } = useContext(ChatContext);
  const [chatName, setChatName] = useState(props.match.params.chatName);
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    setChatName(props.match.params.chatName);
  }, [props.match.params.chatName]);

  useEffect(() => {
    if (JSON.parse(localStorage.getItem(chatName))) {
      setMessages(JSON.parse(localStorage.getItem(chatName)));
    } else {
      setMessages([]);
    }
  }, [chatName]);

  const handleChange = (e) => {
    setMessage(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (message) {
      let newMessages = [...messages];
      newMessages.push({ sender: "me", content: message });
      setMessages(newMessages);
    }
  };

  useEffect(() => {
    if (message) {
      localStorage.setItem(chatName, JSON.stringify(messages));
      setMessage("");
      setTimeout(() => {
        reply();
      }, 2000);
    }
    changeMessage(chatName);
    scrollToBottom();
  }, [messages]);

  const reply = () => {
    let newMessages = [...messages];
    newMessages.push({ sender: chatName, content: "test of reply!" });
    setMessages(newMessages);
    localStorage.setItem(chatName, JSON.stringify(newMessages));
  };

  const renderMessages = () => {
    console.log(messages);
    messages.map((m) => {
      return <div>{m.content}</div>;
    });
  };

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const handleRedirectToDatial = () => {
    props.history.push(`/${chatName}/info`);
  };

  return (
    <div className="messagesList">
      <div onClick={handleRedirectToDatial} className="messagesList__header">
        <div className="messagesList__profile-pic">
          <AccountCircleIcon />
        </div>
        <h2>{chatName}</h2>
      </div>
      <div className="messagesList__chatBox">
        {messages.length > 0 &&
          messages.map((m, index) => (
            <div
              className={`messagesList__card-box ${
                m.sender === "me" ? "messagesList__card-box--me" : ""
              }`}
              key={index}
            >
              <div
                className={`messagesList__card ${
                  m.sender === "me" ? "messagesList__card--me" : ""
                }`}
              >
                {m.content}
              </div>
            </div>
          ))}
        <div ref={messagesEndRef} />
      </div>
      {chatName && (
        <NewMesssage
          onChange={handleChange}
          message={message}
          onSubmit={handleSubmit}
        />
      )}
      {!chatName && (
        <div className="messagesList__empty">Select a chat and talk!</div>
      )}
    </div>
  );
};

export default withRouter(MessagesList);
