import { useState, useEffect, useContext } from "react";
import { NavLink, withRouter } from "react-router-dom";
import ChatContext from "../../context/ChatContext";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const ChatList = (props) => {
  const { chats } = useContext(ChatContext);
  const [activeChatName, setActiveChatName] = useState(null);
  const { isChat } = props;

  useEffect(() => {
    setActiveChatName(props.match.params.chatName);
  }, [props.match.params.chatName]);

  return (
    <div className="chatList">
      {chats.map((chat, index) => {
        return (
          <NavLink
            to={isChat ? `/${chat.name}` : `/${chat.name}/info`}
            key={index}
            className={`chatList__card  ${
              activeChatName === chat.name ? "chatList__card--active" : null
            }`}
          >
            <div className="chatList__profile-pic">
              <AccountCircleIcon />
            </div>
            <div className="chatList__text">
              <h3>{chat.name}</h3>
              {isChat && <h3>{chat.message}</h3>}
            </div>
          </NavLink>
        );
      })}
    </div>
  );
};

export default withRouter(ChatList);
