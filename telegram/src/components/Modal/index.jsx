import React, { useEffect, useState } from "react";
import { createPortal } from "react-dom";
import ChatList from "../../components/ChatList";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

const modalRoot = document.getElementById("modal");

function ContactsModal({ isOpen, children, toggleModal, isChat }) {
  const el = document.createElement("div");

  useEffect(() => {
    modalRoot.appendChild(el);

    return () => {
      modalRoot.removeChild(el);
    };
  }, [el]);

  return createPortal(
    <div onClick={toggleModal} className="modal__container">
      <ArrowBackIcon className="modal__modal-back" />
      {isChat ? (
        <h1 className="modal__modal-title">Chats</h1>
      ) : (
        <h1 className="modal__modal-title">Contacts</h1>
      )}
      <p className="modal__modal">
        <ChatList isChat={isChat} />
      </p>
    </div>,
    el
  );
}

export default ContactsModal;

// import React from "react";
// import { createPortal } from "react-dom";

// export default function ContactsModal() {
//   const el = document.createElement("div");

//   return (
//     <>
//       {createPortal(
//         <div className="modal__container">
//           <div className="modal">Child content</div>
//         </div>,
//         el
//       )}
//     </>
//   );
// }
