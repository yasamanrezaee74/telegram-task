import React from "react";
import ImportContactsIcon from "@material-ui/icons/ImportContacts";
import TextsmsIcon from "@material-ui/icons/Textsms";

const Header = ({ openContacts, openChat }) => {
  return (
    <div className="header">
      <ImportContactsIcon onClick={openContacts} />
      <TextsmsIcon className="header__chats" onClick={openChat} />
    </div>
  );
};

export default Header;
