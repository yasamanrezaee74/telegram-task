import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import ContactDetail from "./containers/ContactDetail";

const Routes = () => {
  return (
    <main>
      <Switch>
        <Route path="/:chatName/Info" component={ContactDetail} />
        <Route path="/:chatName" component={Home} />
        <Route path="/" component={Home} />
      </Switch>
    </main>
  );
};

export default Routes;
